import axios from 'axios'

const axiosInstance = axios.create({
  baseURL: 'https://cors-anywhere.herokuapp.com/https://slash.eadbox.com/api'
})

export const api = {
  get(endpoint) {
    return axiosInstance.get(endpoint)
  },
  post(endpoint, body) {
    return axiosInstance.post(endpoint, body)
  }
}
