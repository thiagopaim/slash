import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/cursos',
      name: 'courses',
      component: () => import('@/views/Courses.vue')
    },
    {
      path: '/cursos/:course',
      name: 'course',
      component: () => import('@/views/Course.vue')
    }
  ],
  mode: 'history'
})
